<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\Listing;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::factory(10)->create();

        Listing::factory(5)->create();

        /*Listing::create([
            'title' => 'Laravel Senior Developer',
            'tags' => 'laravel, javascript, developer',
            'company' => 'Acme Corporation',
            'location' => 'Boston, MA',
            'email' => 'email@me.com',
            'website' => 'acme.com',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras eu mi ac mauris aliquam tempus viverra eget urna. Nulla tincidunt bibendum nibh, et ullamcorper tellus consectetur sit amet.'
        ]);

        Listing::create([
            'title' => 'NodeJS Senior Developer',
            'tags' => 'nodejs, javascript, developer',
            'company' => 'Davies Paints Corporation',
            'location' => 'Pasig, 1604',
            'email' => 'email@me.com',
            'website' => 'davies.com',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras eu mi ac mauris aliquam tempus viverra eget urna. Nulla tincidunt bibendum nibh, et ullamcorper tellus consectetur sit amet.'
        ]);*/

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);
    }
}
